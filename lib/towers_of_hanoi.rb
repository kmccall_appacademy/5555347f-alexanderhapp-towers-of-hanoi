# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  @@disk_hash = { 1 => "-", 2 => "--", 3 => "---", nil => "" }

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def move(from, to)
    @towers[to] << @towers[from].pop
  end

  def valid_move?(from, to)
    return false unless (0..2).cover?(from) && (0..2).cover?(to)
    if @towers[from].empty?
      false
    elsif !@towers[to].empty? && @towers[from].last > @towers[to].last
      false
    else
      true
    end
  end

  def won?
    @towers[1].count == 3 || @towers[2].count == 3
  end

  def play
    display_title
    until won?
      display_towers
      from, to = get_move
      if valid_move?(from, to)
        move(from, to)
      else
        invalid_move
      end
    end
    you_won
  end

  private

  def get_move
    puts "\nWhich tower would you like to select a disc from?\n"
    from = get_input
    puts "\nWhich tower would you like to move the disc to?\n"
    to = get_input
    [from, to]
  end

  def get_input
    gets.chomp.to_i - 1
  end

  def display_title
    puts "\n*********************"
    puts "*  TOWERS OF HANOI  *"
    puts "*********************\n\n"
  end

  def display_towers
    puts "\nTower 1   Tower 2   Tower 3\n"
    puts @@disk_hash[@towers[0][2]].rjust(7) + "   " + @@disk_hash[@towers[1][2]].rjust(7) + "   " + @@disk_hash[@towers[2][2]].rjust(7)
    puts @@disk_hash[@towers[0][1]].rjust(7) + "   " + @@disk_hash[@towers[1][1]].rjust(7) + "   " + @@disk_hash[@towers[2][1]].rjust(7)
    puts @@disk_hash[@towers[0][0]].rjust(7) + "   " + @@disk_hash[@towers[1][0]].rjust(7) + "   " + @@disk_hash[@towers[2][0]].rjust(7)
  end

  def invalid_move
    puts "\nInvalid Move! Please try again!\n"
  end

  def you_won
    display_towers
    puts "\nCongratulations! You've won!\n\n"
    "Game Over"
  end

end
